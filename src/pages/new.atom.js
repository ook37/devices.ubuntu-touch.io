import feedGenerator from "@logic/build/generateRss.js";

export async function get() {
  const feed = await feedGenerator("feed-generate");

  return {
    body: feed.atom1()
  };
}
