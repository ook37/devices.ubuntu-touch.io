---
name: "Meizu MX4"
deviceType: "phone"
description: "Meizu MX4 was a device for early adopters, and it is still delivers considerably nice computing power; it runs on a Meizu-customised octa-core MediaTek MT6595 SoC with four ARM Cortex-A17 and four ARM Cortex-A7 cores, with a PowerVR G6200 GPU to handle the graphics, all supported by 2GB of LPDDR3 RAM, it has a 5.36-inch (diagonal) display with a resolution of 1,920 by 1,152 pixels, giving a pixel density of 418ppi."
subforum: "96/meizu-mx4"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x2.2 GHz Cortex-A17 & 4x1.7 GHz Cortex-A7)"
  - id: "chipset"
    value: "MediaTek MT6595"
  - id: "gpu"
    value: "PowerVR G6200"
  - id: "rom"
    value: "16GB"
  - id: "ram"
    value: "2GB"
  - id: "android"
    value: "4.4.2"
  - id: "battery"
    value: "3100 mAh"
  - id: "display"
    value: "1152x1920 pixels, 5.36 in"
  - id: "rearCamera"
    value: "20.7MP"
  - id: "frontCamera"
    value: "2.0MP"

sources:
  portType: "external"

externalLinks:
  - name: "Kernel source"
    link: "https://github.com/ubports/kernel_arale"

seo:
  description: "Switch your Meizu MX4 smartphone OS to Ubuntu Touch, a privacy focused operating system, developed by hundreds of volunteers around the world."
  keywords: "Ubuntu Touch, Meizu MX4, linux for smartphone, Linux on phone"
---
