---
name: "Fairphone 4"
deviceType: "phone"
buyLink: "https://shop.fairphone.com/en/buy-fairphone-4"
description: "The Fairphone 4 in combination with Ubuntu Touch is currently the best option, if you value sustainability, fair trade materials, repairability and independency from Google and the Android operating systems."
tag: "promoted"
subforum: "105/fairphone-4"
price:
  avg: 579
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (2x2.2 GHz Kryo 570 & 6x1.8 GHz Kryo 570)"
  - id: "chipset"
    value: "Qualcomm SM7225 Snapdragon 750G 5G"
  - id: "gpu"
    value: "Adreno 619"
  - id: "rom"
    value: "128GB/256MB"
  - id: "ram"
    value: "6GB/8GB"
  - id: "android"
    value: "Android 11"
  - id: "battery"
    value: "3900 mAh, removable"
  - id: "display"
    value: "1080 x 2340 pixels, 6.3 in"
  - id: "rearCamera"
    value: "48 MP f/1.6,(wide), 48 MP f/2.2 (ultrawide)"
  - id: "frontCamera"
    value: "25 MP f/2.2 (wide)"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "75.5 x 162 x 10.5 mm"
  - id: "weight"
    value: "225g"
  - id: "releaseDate"
    value: "October 2021"

communityHelp:
  - name: "Telegram - Ubuntu Touch on Fairphone 4"
    link: "https://t.me/ut_on_fp4"

sources:
  portType: "reference"
  portPath: "android11"
  deviceGroup: "fairphone-4"
  deviceSource: "fairphone-fp4"
  kernelSource: "kernel-fairphone-sm7225"

contributors:
  - name: "Fairphone"
    photo: "https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/uploads/1ac3b695157a14a676a32aa1221b62c7/R0Ws6Pao_400x400.jpg"
    forum: "https://forum.fairphone.com/t/fp4-ubuntu-touch/80816"
    role: "Phone maker"
  - name: "Muhammad"
    forum: "https://forums.ubports.com/user/thevancedgamer"
    role: "Maintainer"
    renewals:
      - "2023-03-23"
  - name: "fredldotme"
    forum: "https://forums.ubports.com/user/fredldotme"
    photo: "https://forums.ubports.com/assets/uploads/profile/2070-profileavatar.png"
    role: "Developer"
  - name: "Flohack"
    forum: "https://forums.ubports.com/user/Flohack"
    photo: "https://forums.ubports.com/assets/uploads/profile/414-profileavatar.png"
    role: "Contributor"
  - name: "TheKit"
    forum: https://forums.ubports.com/user/thekit
    role: "Contributor"
  - name: "z3ntu"
    role: "Developer"

seo:
  description: "Flash your Fairphone 4 with the latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, Fairphone4, Fairphone 4, Privacy Phone, Sustainable Phone, Fair Phone"
---
