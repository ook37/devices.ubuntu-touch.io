---
name: "Xiaomi Redmi Note 3 (kenzo)"
deviceType: "phone"
description: "Released in November 2015, the Xiaomi Redmi Note 3 aka kenzo is one of the best value for money mid-ranger of its time with a massive  4000mAh battery , it also had a 5.5 inch IPS LCD display, Hexa-core Snapdragon 650 processor with 2 big cores for perfomence and 4 little cores for efficiency , and comes with either 2GB of RAM and 16GB internal storage, or 3GB of RAM with 32GB internal storage. The main shooter is 16MP with an f/2.2 aperture, while the front camera is 5MP with f/2.0 aperture."
price:
  avg: 200
subforum: "103/xiaomi-redmi-note-3"

deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "arch"
    value: "arm64"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "4000 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "16 MP"
  - id: "frontCamera"
    value: "5 MP"
  - id: "dimensions"
    value: "150 x 76 x 8.7 mm (5.91 x 2.99 x 0.34 in)"
  - id: "weight"
    value: "164 g (5.78 oz)"
contributors:
  - name: "mathew-dennis"
    forum: "https://github.com/mathew-dennis"
    photo: "https://gitlab.com/uploads/-/system/user/avatar/8316571/avatar.png?width=400"
communityHelp:
  - name: "Telegram"
    link: "https://t.me/utkenzo"
sources:
  portType: "external"
  issuesLink: "https://github.com/mathew-dennis/device_kenzo_halium9/issues"
externalLinks:
  - name: "Source"
    link: "https://github.com/mathew-dennis"
---
